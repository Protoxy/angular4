export class Element {

  constructor(title, url) {
    this.title = title;
    this.url = url;
  }
  title: string;
  url: string;
}
