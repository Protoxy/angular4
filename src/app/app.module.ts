/*
* Module that hold all other components
* */
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpModule} from '@angular/http';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {LiveComponent} from './live/live.component';
import {PlanComponent} from './plan/plan.component';

@NgModule({
  declarations: [
    AppComponent,
    LiveComponent,
    PlanComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
        path: 'live',
        component: LiveComponent
      },
      {
        path: 'plan',
        component: PlanComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
