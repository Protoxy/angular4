/**
 * Contains the ref to angular core
 * A reference to this component is defined in the app.module.ts
 */
import {Component, OnInit} from '@angular/core';
import {Element} from '../models/element.model';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.css']
})
export class LiveComponent implements OnInit {
  title = 'LIVE';
  form;
  array = [new Element('Jean', 'jean'), new Element('Drag', 'drg'), new Element('FireB', 'firb')];

  constructor() {
  }

  ngOnInit() {
    this.form = new FormControl({});
  }

}

